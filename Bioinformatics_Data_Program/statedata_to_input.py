import ast
import sys
import subprocess
import itertools
import os


class statedata_to_input(object):
    """Make an input file from hmm track and/or gc data and/or ctcf site data"""
    def __init__(self, inputfile, outputfile_cond, outputfile_beads, bead_info = ['start', 'end', 3000, 90],
                 prot_info = [2, 1000, 100], state_col_info = [1, 2, 3], match_ids=[[13], [1,4,5], [9,10]],
                 match_names = ['htr', 'str', 'weak'], override = ['weak', 'str'], gcfile = None, no_data_val = 0,
                 ctcf_file = None, ctcf_col_info = [0,1,2,5], ctcf_info = [1,10.0], ctcf_style = 'point',
                 htr_calc_info = None):

        # A list of states from a hmm track input file
        self.statedata = []

        # A dict mapping hmm track numbers to their names
        self.statenames = dict(zip(match_names, match_ids))
        self.stateindex = dict(zip(match_names, range(len(match_names))))

        # If the file has been modified and columns are different to normal, set them here.
        self.start_bp_col = state_col_info[0]
        self.end_bp_col = state_col_info[1]
        self.chr_state_col = state_col_info[2]

        # Give the variables in bead_info more descriptive names
        self.start_bp = bead_info[0]
        self.end_bp = bead_info[1]
        self.bin_size = bead_info[2]
        self.bin_thr = bead_info[3]
        self.htr_list = []

        # Write a file with info about the input conditions
        logfile = open('logfile.txt','w')

        # Use shell commands head or tail to read the start and end bp value, if not already specified
        if self.start_bp == 'start':
            headproc = subprocess.Popen(["head", "-n", "2", inputfile], stdout=subprocess.PIPE)
            self.start_bp = int(subprocess.check_output(["tail","-n","1"],
                                                        stdin=headproc.stdout).split()[self.start_bp_col])
            headproc.terminate()
        if self.end_bp == 'end':
            tailproc = subprocess.Popen(["tail", "-n", "1", inputfile], stdout=subprocess.PIPE)
            self.end_bp = int(tailproc.stdout.read().split()[self.end_bp_col])
            tailproc.terminate()
        logfile.write('Start Bp: '+str(self.start_bp)+' End Bp: '+str(self.end_bp)+'\n')

        # Find the number of bins in the input file
        self.bin_no = (self.end_bp-self.start_bp)/self.bin_size
        chr_in = open(inputfile, 'r')
        for line in chr_in:
            tokens = line.split()
            # Ignore commented out lines
            if '#' in tokens[0]:
                pass
            # Read in data within specified range
            elif int(tokens[self.start_bp_col]) < self.end_bp and int(tokens[self.end_bp_col]) > self.start_bp:
                self.statedata.append([int(tokens[self.start_bp_col]), int(tokens[self.end_bp_col]),
                                       int(tokens[self.chr_state_col].split('_')[0])])
        chr_in.close()
        # Variable for binned state data
        self.binned_statedata = [[] for i in range(self.bin_no)]
        self.state_bin_data(match_ids, gcfile)

        htr_prop_from_state = self.htr_perc_from_state(inputfile,htr_calc_info)
        logfile.write('Heterochromatin proportion: ' + str(htr_prop_from_state)+' Heterochromatin Beads: '+
                      str(int(htr_prop_from_state*self.bin_no))+'/'+str(self.bin_no)+'\n')

        # If you submit a gc file heterochromatin beads are assigned based on this
        if gcfile is not None:
            gcdata = []
            # If there is no gc data for sections of the genome (usually the centromere), set the gc-percentage to
            # no_data_val for those regions. Setting no_data_val = 0 will make those beads heterchromatic (low gc)
            for line in open(gcfile, 'r'):
                if '#' in line[0]:
                    pass
                else:
                    [bp, perc] = line.split()
                    if self.start_bp < int(bp) < self.end_bp:
                        gcdata.append([int(bp), float(perc)])
                    # Stop reading after end_bp
                    elif int(bp) > self.end_bp:
                        break

            # self.binned_gcdata = [start_bp,percentage of gc reads,flag if no data was available]
            self.binned_gcdata = [[0, 0, 0] for i in range(self.bin_no+1)]
            self.gc_bin_data(gcdata, no_data_val)
            threshold = self.match_threshold(htr_prop_from_state)
            
        # Assign a value for ctcf interactions to each bead, if using ctcf data
        if ctcf_file is not None:
            ctcf_chr_col = ctcf_col_info[0]
            ctcf_start_col = ctcf_col_info[1]
            ctcf_end_col = ctcf_col_info[2]
            ctcf_dir_col = ctcf_col_info[3]
            ctcfdata = []
            chromosome_no = ctcf_info[0]

            self.stateindex['ctcf'] = len(self.stateindex)
            if ctcf_style == 'point':
                self.stateindex['ctcf-2'] = len(self.stateindex)

            # print ctcf_info, self.start_bp, self.end_bp
            for line in open(ctcf_file, 'r'):
                tokens = line.split()
                if '#' in tokens[0]:
                    pass
                else:
                    # Check if peak is in area we are interested in, as well as in correct chromosome
                    if int(tokens[ctcf_start_col]) < self.end_bp and int(tokens[ctcf_end_col]) > self.start_bp and \
                                    tokens[ctcf_chr_col] == 'chr'+str(chromosome_no):
                        if int(tokens[ctcf_dir_col]) == 1 or int(tokens[ctcf_dir_col]) == -1:
                            ctcfdata.append([int(tokens[ctcf_start_col]), int(tokens[ctcf_end_col]),
                                             int(tokens[ctcf_dir_col])])
            self.binned_ctcf_list = [0 for i in range(self.bin_no+1)]
            ctcf_sites = self.ctcf_bin_data(ctcfdata, self.bin_size, self.start_bp, ctcf_style)
            logfile.write('Ctcf Beads:'+str(ctcf_sites)+'/'+str(self.bin_no)+'\n')

        logfile.write('{State Descriptions: Ids}\n'+str(self.statenames)+'\n')
        self.write_in=open(outputfile_cond,'w')
        
        if gcfile is None and ctcf_file is None:
            self.write_in.write('# From input file: '+inputfile+'\n')
        elif ctcf_file is None:
            self.write_in.write('# From input files: '+inputfile+' '+gcfile+'\n')
        elif gcfile is None:
            self.write_in.write('# From input files: '+inputfile+' '+ctcf_file+'\n')
        else:
            self.write_in.write('# From input files: '+inputfile+' '+gcfile+' '+ctcf_file+'\n')
            
        self.write_globals(outputfile_beads, prot_info)
        dna_types,features,count_dict = self.write_bead_colour_file(outputfile_beads, override)
        self.write_section(self.bin_no)
        self.write_boundaries()
        if ctcf_file is None:
            self.write_bridges(prot_info, dna_types)
        else:
            self.write_bridges(prot_info, dna_types)
        self.write_in.close()
        logfile.write('There are '+str(dna_types)+' dna bead types\n')
        # print self.statenames,self.stateindex,features
        self.stateindex_rev=dict()
        for k in self.stateindex:
            self.stateindex_rev[self.stateindex[k]] = k
        stringlist=[]
        for k in features:
            typestring = ''
            count = int(count_dict[k])
            for type in k:
                typestring += self.stateindex_rev[type]+' '
            datastring = 'Bead type '+str(features[k])+' is '+typestring+' ('+str(count)+')'
            stringlist.append([features[k],datastring])
        stringlist.sort()
        for id,string in stringlist:
            logfile.write(string+'\n')

    
    def ctcf_bin_data(self, ctcfdata, bin_size, start_bp, ctcf_style='point'):
        """Create ctcf-labelled beads from an upstream to a downstream ctcf site"""
        if ctcf_style == 'landingstrip':
            for i in range(len(ctcfdata)-1):
                if ctcfdata[i][2] == 1 and ctcfdata[i+1][2] == -1:
                    start_bin = (ctcfdata[i][1]-start_bp)/bin_size
                    end_bin = (ctcfdata[i+1][0]-start_bp)/bin_size
                    for j in range(start_bin, end_bin):
                        self.binned_ctcf_list[j] = 1
            for ctcf_value, data_value in zip(self.binned_ctcf_list, self.binned_statedata):
                if ctcf_value == 1:
                    data_value[1].append(self.stateindex['ctcf'])
        elif ctcf_style == 'point' or ctcf_style == 'point-nodirection':
            for ctcfst, ctcfend, ctcfdir in ctcfdata:
                to_next_bin = self.bin_size - ((ctcfst - self.start_bp) % self.bin_size)
                if(to_next_bin > (ctcfend - ctcfst)):
                    bin_length = 1
                else:
                    bin_length = (ctcfend - ctcfst - to_next_bin)/self.bin_size + 2
                start_bin = int((ctcfst -self.start_bp)/self.bin_size)

                # Proportion of each section which lies in the range of a bin [amount in bp,bin id]
                prop_in_bin = []
                for j in range(bin_length):
                    prop_in_bin.append([min(self.start_bp+(start_bin+1+j)*self.bin_size-ctcfst,
                                            ctcfend-(start_bin+j)*self.bin_size-self.start_bp, self.bin_size),
                                        start_bin+j])

                for bp, bin_id in prop_in_bin:
                    # Sometimes the final section will extend beyond the end_bp value you set for the simulation. This
                    # prevents any attempts to write to those (non-existant!) bins
                    if bin_id > self.bin_no-1:
                        break
                    # If enough of the section overlaps a bin, label it.
                    if (bp > self.bin_thr):
                        self.binned_ctcf_list[bin_id] = ctcfdir
            for ctcf_value, data_value in zip(self.binned_ctcf_list, self.binned_statedata):
                if (ctcf_value == 1 or ctcf_value == -1) and ctcf_style == 'point-nodirection':
                    data_value[1].append(self.stateindex['ctcf'])
                elif ctcf_value == 1 and ctcf_style == 'point':
                    data_value[1].append(self.stateindex['ctcf'])
                elif ctcf_value == -1 and ctcf_style == 'point':
                    data_value[1].append(self.stateindex['ctcf-2'])

        return sum(map(lambda a:abs(a),self.binned_ctcf_list))

    def depth(self, list_input):
        function = lambda L: isinstance(L, list) and max(map(self.depth, L))+1
        return(function(list_input))
    
    def gc_bin_data(self, data, no_data_val):
        """The gc file format gives gc reads in (usually) 5 bp blocks"""
        for block_start,perc in data:
            bin_no = (block_start - self.start_bp)/self.bin_size
            self.binned_gcdata[bin_no][0] += perc
            self.binned_gcdata[bin_no][1] += 1
        for i in range(len(self.binned_gcdata)):
            if self.binned_gcdata[i][1] == 0:
                self.binned_gcdata[i][0] = no_data_val
                self.binned_gcdata[i][2] = 1
            else:
                self.binned_gcdata[i][0] = float(self.binned_gcdata[i][0])/self.binned_gcdata[i][1]
        self.binned_gcdata = [[i, a[0], a[2]] for i, a in enumerate(self.binned_gcdata, 1)]
        
    def htr_perc_from_state(self, inputfile, htr_calc_info):
        """Find the percentage of heterochromatin from the hmm input file"""
        start_bp = self.start_bp
        end_bp = self.end_bp
        if htr_calc_info is not None:
            start_bp = htr_calc_info[0]
            end_bp = htr_calc_info[1]
        total_htr_bp = 0
        for line in open(inputfile,'r'):
            tokens = line.split()
            # Ignore commented lines
            if '#' in tokens[0]:
                pass
            elif int(tokens[self.chr_state_col].split('_')[0]) in self.statenames['htr'] and \
                            int(tokens[self.start_bp_col]) < end_bp and \
                            int(tokens[self.end_bp_col]) > start_bp:
                if int(tokens[self.end_bp_col]) > end_bp:
                    total_htr_bp += (self.end_bp-int(tokens[self.start_bp_col]))
                elif(int(tokens[self.start_bp_col]) < start_bp):
                    total_htr_bp += (int(tokens[self.end_bp_col])-start_bp)
                else:
                    total_htr_bp += (int(tokens[self.end_bp_col])-int(tokens[self.start_bp_col]))
        htr_prop = float(total_htr_bp)/float(end_bp-start_bp)
        return htr_prop
    
    def match_threshold(self, htr_perc_from_state):
        """Find gc threshold level where we have a number of heterochromatin
        beads equal to the bead no from state data only.
        Arguments:
        htr_perc_from_state -- The heterochromatin percentage calaculated from hmm state data.
        """
        curr_thr = 50.0
        thr = 50.0
        for i in range(15):
            htr_list = []
            # print thr,curr_thr
            for bin, perc, nodata_flag in self.binned_gcdata:
                if perc < curr_thr and nodata_flag == 0:
                    htr_list.append([bin, 1, 1])
                elif perc < curr_thr and nodata_flag == 1:
                    htr_list.append([bin, 1, 0])
                else:
                    htr_list.append([bin, 0, 1])
            # Calculate the percentage of heterochromatin beads for this threshold
            htr_list_finalcalc = [htr*nodata_flag for bin,htr,nodata_flag in htr_list]
            htr_list_prop = float(sum(htr_list_finalcalc))/len(htr_list_finalcalc)
            # print htr_list_prop,htr_perc_from_state
            if(htr_list_prop > htr_perc_from_state):
                curr_thr = curr_thr - 25.0/(2.0**i)

            else:
                curr_thr = curr_thr + 25.0/(2.0**i)
        # print str(curr_thr)+'thr'
        self.htr_list = htr_list
        # Delete nodata_flag
        for value1, value2 in zip(self.binned_gcdata, htr_list):
            value1.pop(2)
            value2.pop(2)
        if 'htr' not in self.stateindex:
            self.stateindex['htr'] = len(self.stateindex)
        for htr_value, data_value in zip(htr_list, self.binned_statedata):
            if htr_value[1] == 1:
                data_value[1].append(self.stateindex['htr'])
                data_value[1].sort()

        return curr_thr

    def state_bin_data(self, match_ids, gcfile):
        """Map the information in the state data file to the beads in the simulation"""

        # Keep track of which identifiers go together. i.e [[1,2],3] -> {1:0,2:0,3:1}
        group_dict = dict()
        for group_no, group_items in enumerate(match_ids):
            for item in group_items:
                group_dict[item] = group_no
        # print group_dict

        # If we are using a gc file to assign heterochromatin beads, don't assign them via hmm track data
        if gcfile is not None:
            try:
                match_ids.remove(self.statenames['htr'])
            except KeyError:
                pass
        # print match_ids

        # Remove the groups in our match list, so we can iterate over it. i.e. [[1,2],3,[6,7]] -> [1,2,3,6,7]
        match_copy = match_ids[:]
        match_ids = []

        for x in match_copy:
            match_ids.extend(x)
        
        for sectionstart_bp, sectionend_bp, chrtype in self.statedata:
            if chrtype in match_ids:
                to_next_bin = self.bin_size - ((sectionstart_bp - self.start_bp) % self.bin_size)
                if(to_next_bin > (sectionend_bp - sectionstart_bp)):
                    bin_length = 1
                else:
                    bin_length = (sectionend_bp - sectionstart_bp - to_next_bin)/self.bin_size + 2
                start_bin = int((sectionstart_bp-self.start_bp)/self.bin_size)

                # Proportion of each section which lies in the range of a bin [amount in bp,bin id]
                prop_in_bin = []
                for j in range(bin_length):
                    # This is either from the start of the section to the start of the next bin, from the start of
                    # the bin to the end of the section, or just the entire bin
                    prop_in_bin.append([min(self.start_bp+(start_bin+1+j)*self.bin_size-sectionstart_bp,
                                            sectionend_bp-(start_bin+j)*self.bin_size-self.start_bp, self.bin_size),
                                        start_bin+j])
                        
                for bp, bin_id in prop_in_bin:
                    # Sometimes the final section will extend beyond the end_bp value you set for the simulation. This
                    # prevents any attempts to write to those (non-existant!) bins
                    if bin_id > self.bin_no-1:
                        break
                    # If enough of the section overlaps a bin, label it.
                    if (bp > self.bin_thr):
                        self.binned_statedata[bin_id].append(group_dict[chrtype])

        # Label the binned state data from 1 -> no. beads rather than from 0 -> no. beads - 1 and remove duplicate
        # chrtypes.
        for i in range(len(self.binned_statedata)):
            self.binned_statedata[i]=[i+1, list(set(self.binned_statedata[i]))]

    def write_bead_colour_file(self, filename, override):
        with open(filename, 'w') as w:
            features = {():1}
            count = 1
            for i in range(1, len(self.stateindex)+1):
                for j in itertools.combinations([l for l in range(len(self.stateindex))], i):
                    count += 1
                    features[j] = count
            # print features, override, self.stateindex[override[0]], self.stateindex[override[1]]
            # Convert to nested list if there is only one type to override
            if self.depth(override) == 1:
                override = [override]
            for orig,replace in override:
                for key in features:
                    if self.stateindex[orig] in key and self.stateindex[replace] in key:
                        keylist = list(key)
                        keylist.remove(self.stateindex[orig])
                        new_key=tuple(keylist)
                        features[key] = features[new_key]
            # print features, override, self.stateindex[override[0][0]],self. stateindex[override[0][1]]
            # Delete unused feature combinations
            count_dict = dict()
            for k in features:
                count_dict[k]=0
            for atom_data in self.binned_statedata:
                count_dict[tuple(atom_data[1])] += 1
            for k in count_dict:
                if(count_dict[k] == 0):
                    features.pop(k)

            # Since the combinations may not have consecutive atom numbers,rewrite them so they do
            valuelist = []
            for key in features:
                valuelist.append(features[key])
            unique_values = list(set(valuelist))
            for i in range(len(unique_values)):
                unique_values[i]=[unique_values[i],i+1]
            for key in features:
                value = features[key]
                for pair in unique_values:
                    if value == pair[0]:
                        features[key] = pair[1]

            for atom_data in self.binned_statedata:
                if atom_data[1] is []:
                    w.write(str(atom_data[0])+' 1\n')
                else:
                    w.write(str(atom_data[0])+' '+str(features[tuple(atom_data[1])])+'\n')
        return max(zip(*unique_values)[1]),features,count_dict



    def write_bridges(self, prot_info, dna_types):
        for i in range(prot_info[0]):
            self.write_in.write('name=prot'+str(i+1)+' style=prot atom_no='+str(prot_info[i+1])+' atom_type='+
                                str(dna_types+i+1)+'\n')

    def write_boundaries(self):
        self.write_in.write('name=boundary'+' style=bdry-shift dynamic_boundaries=150,150,150\n')
        
    def write_section(self,atom_no):
        self.write_in.write('name=dna style=linear start_point=0,0,0 atom_no='+str(atom_no)+
                            ' section_length=1 atom_type=1 in_bounds=False\n')

    def write_globals(self,outputfile_beads,prot_info):
        self.write_in.write('all boundaries=-75,75,-75,75,-75,75')
        self.write_in.write(' order=dna,boundary')
        for i in range(prot_info[0]):
            self.write_in.write(',prot'+str(i+1))
        self.write_in.write(' switch_data='+str(outputfile_beads)+'\n')


if __name__ == '__main__':

    def convert_input(input_string):
        """Convert an input string into a list of int, float and string types"""
        # Remove outer brackets
        input_string = input_string.strip('[')
        input_string = input_string.strip(']')
        output_list = []

        # Split into individual entries and check type. int -> float ->string should work ok.
        tokens = input_string.split(',')
        for token in tokens:
            try:
                token = int(token)
            except ValueError:
                try:
                    token = float(token)
                except ValueError:
                    token = str(token)
            output_list.append(token)
        return output_list


    if len(sys.argv) < 4:

        print '\n'
        print 'Usage: python statedata_to_input.py Chromatin state inputfile,'
        print 'Conditions outputfile, Bead type outputfile, Optional arguments\n'
        print 'Optional Arguments:\n'
        print 'bead_info - [Start Bp, End bp, Bin Size, Bin Threshold],'
        print 'prot_info - [No. of protein types, No. of type 1 proteins, No. of type 2 proteins, ...]'
        print 'state_col_info - [Start bp column, End bp column, Chromatin state column]'
        print 'match_ids - [Id\'s in Chromatin state inputfile for group 1, group 2, ...]'
        print 'match_names - [Name of group 1, Name of group 2, ...]'
        print 'override - [[Group name to be replaced, Group name to replace with], ...]'
        print 'gcfile - Name of gc input file, no_data_val - Value assigned for missing data'
        print 'htr_calc_info - [Start bp for heterochromatin percentage calculation, End bp]'
        print 'ctcf_file - Name of ctcf input file'
        print 'ctcf_info - [Chromosome no., Min. peak score to be included]'
        print 'ctcf_style - landingstrip, point or point-nodirection'
        print 'ctcf_col_info - [Chromosome no. column, Start bp column, End bp column, Ctcf direction column]'
        print 'Optional arguments should be written as bead_info=[start,200000,1000,90]'
        print '\n'
        quit()
    inputfile = sys.argv[1]
    cond_out = sys.argv[2]
    bead_out = sys.argv[3]
    opt_dict = dict()
    for i in range(4, len(sys.argv), 1):
        key,val = sys.argv[i].split('=')
        if '[' in val:
            # val = ast.literal_eval(val) - literal_eval can't deal with lists of mixed type
            val = convert_input(val)
        opt_dict[key]=val
    statedata_to_input(inputfile, cond_out, bead_out,**opt_dict)
