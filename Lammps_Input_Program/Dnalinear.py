import vector
import Dna
import RewindableIterator

class Dnalinear(Dna.Dna):

    def __init__(self,atom_no,boundaries,boundary_style=None,other_atoms=[],atom_type=1,switch_data=[],
                   start_point=None,start_point_minus_one=None,atom_diameter=1,section_length=1,connect=False,
                   in_bounds=True,overlap=False,style='linear',name='dnalinear'):
        super(Dnalinear,self).__init__(atom_no,boundaries,boundary_style,other_atoms,atom_type,switch_data,
                                        start_point,start_point_minus_one,atom_diameter,section_length,connect,
                                        in_bounds,overlap,style,name)
        # Variables specific to Dnalinear
        self.atomdict={'id':0,'mol':1,'type':2,'x':3,'y':4,'z':5}

        self.section_length_check()
        
        # Initial section and vector
        
        self.first_section(other_atoms)
        
        # Main Loop
        
        self.mainloop(other_atoms)

        self.assign_all(atom_no,connect)
        self.switch_atom_types(switch_data)
        
            
    def mainloop(self,other_atoms):
        """Creates all components of a DNA chain. Will restart if enough failures occur."""
        try_no=1

        def next(current_atom_no,other_atoms,section_length):
            new_section=[]
            current_position=vector.vector(self.atoms[current_atom_no-1][self.atomdict['x']],self.atoms[current_atom_no-1][self.atomdict['y']],self.atoms[current_atom_no-1][self.atomdict['z']])
            if(current_atom_no!=1):
                previous_position=vector.vector(self.atoms[current_atom_no-2][3],self.atoms[current_atom_no-2][4],self.atoms[current_atom_no-2][5])
                vct=vector.vector.random_vector_selfavd(current_position,previous_position)
            else:
                vct=vector.vector.random_vector(self.atom_diameter)
            for i in range(section_length):
                current_position=vector.vector.add(current_position,vct)
                new_section.append([current_atom_no+i+1,1,self.atom_type,current_position.x,current_position.y,current_position.z])
            self.add_section(new_section,vct)
            return new_section,vct
        """Since we have placed only 1 section,list runs to self.completed_section_no"""
        start_atom_id_list=[self.section_length*i for i in range(1,self.completed_section_no)]
        atom_start_iter=RewindableIterator.RewindableIterator(start_atom_id_list)
        for start_id in atom_start_iter:
            section_length=min(self.section_length,self.completed_atom_no-start_id)
            new_section,vct_temp=next(start_id,other_atoms,section_length)
            end_id=start_id+section_length
            if(self.overlap_allowed==True and int(self.in_boundary(new_section))*int(self.in_bounds)==int(self.in_bounds)):
                """Success, but only because there's nothing really to go wrong (apart from being out of boundaries)"""
                try_no=1
            elif(int(self.in_boundary(new_section))*int(self.in_bounds)==int(self.in_bounds) and
               self.overlap(start_id,end_id,other_atoms)==False):
                """Successfully placed atoms within overlap/boundary restrictions"""
                try_no=1
                #print 'success'+str(new_section)
            elif(int(self.in_boundary(new_section))*int(self.in_bounds)!=int(self.in_bounds) or
               self.overlap_allowed==False and self.overlap(start_id,end_id,other_atoms)==True):
                """Failed to place atoms for a particular section. Remove section and vector from list."""
                #print 'fail'+str(new_section)+str(self.atoms[-1])
                self.remove_section(new_section,vct_temp)
                try_no=try_no+1
                if (try_no>4*len(self.vectors)):
                    """Unsucessful after several tries, restart whole DNA object"""
                    print 'Failed at {0:d} atoms'.format(len(self.atoms))
                    self.atoms=[]
                    self.vectors=[]
                    self.atom_no=0
                    self.first_section(other_atoms)
                    atom_start_iter.seek(0,True)
                else:
                    """Unsuccessful, try again with alternative vector (if available)"""
                    atom_start_iter.repeat() 
    