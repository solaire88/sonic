import warnings
class RewindableIterator(object):
    """An iterator which can go backwards,forwards or jump to a particular iterable.
    Takes a regular iterable as argument"""
    def __init__(self,iterable):
        self.iterable=iterable
        self.index=0
        
    def __iter__(self):
        """Start iterating"""
        self.index=0
        return self
    
    def next(self):
        """Return this item, increment index counter so further next() calls return next items"""
        try:
            value=self.iterable[self.index]
            self.index+=1
            return value
        except IndexError:
            raise StopIteration
            
    def prev(self):
        """Return previous item on next iteration"""
        if self.index==1:
            warnings.warn("Tried to get previous item at beginning of iterator. Returned first item of iterable instead.")
            self.index=0
        else:
            self.seek(-1)
        
    def repeat(self):
        """Return current item on next iteration"""
        self.seek(0)
        
    def seek(self,shift,absolute=False):
        """Move either to particular index with absolute=True, or shift index up/down with absolute=False (default).
        Does not return an item. Shift=0,absolute=False gets same item again on next iteration"""
        if(type(shift) is not int):
            raise TypeError
        
        if absolute:
            self.index=shift
        else:
            self.index+=shift-1
        
        if(self.index<0):
            self.index=0
            warnings.warn("Tried to get item with index <0. Returned item at index 0.")
        
    def update(self,new_value,index='current'):
        """Replace a value of the iterator with a new value. Index points to the next
        index to be read, not the one currently in use"""
        if index=='current':
            index=self.index-1
        self.iterable[index]=new_value
    
