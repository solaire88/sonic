#!/bin/bash

# A script which writes lammps input files for the region 152600262-158600262 bp
# of chr7. Shh is about midway along this region! 

Bioinfprog="/Disk/ball_staging/s0841882/Shared/Bioinformatics_Data_Program/statedata_to_input.py"
Statefile="/Disk/ball_staging/s0841882/Shared/chr7_state.txt"
Gcfile="/Disk/ball_staging/s0841882/Shared/chr7_gc.txt"
Ctcffile="/Disk/ball_staging/s0841882/Shared/ctcf_peak_details.txt"
Inputcond="/Disk/ball_staging/s0841882/Shared/Program_Outputs/chr7_conditions.txt"
Beadlist="/Disk/ball_staging/s0841882/Shared/Program_Outputs/chr7_beadlist.txt"
Logfile="/Disk/ball_staging/s0841882/Shared/Program_Outputs/chr7_logfile.txt"
Lammpsinprog="/Disk/ball_staging/s0841882/Shared/Lammps_Input_Program/Dnamain.py"


# To see the options for the Bioinformatics Data Program, run it without any
# arguments
python ${Bioinfprog} ${Statefile} ${Inputcond} ${Beadlist} bead_info=[152600262,158600262,3000,90] prot_info=[2,200,20] state_col_info=[2,3,4] gcfile=${Gcfile} ctcf_file=${Ctcffile} ctcf_info=[7,0.0] ctcf_style=point-nodirection

mv logfile.txt ${Logfile}
for i in {1..10..1}
do
Lammpsout="/Disk/ball_staging/s0841882/Shared/Program_Outputs/Lammps_Input."$i".in"
python ${Lammpsinprog} ${Inputcond} ${Lammpsout}
done
mv dump.* Lammps_Outputs
mv random.* Lammps_Outputs

