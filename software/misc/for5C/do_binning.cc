//**************************************************
// Program to re bin the 5C data

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>

using namespace std;

int bp2bin(const long int &mid1,const long int &start,const int &bw);

int main() {

  string infile="../interactions_limb_expressing.bed",
    oufile="bin10kb_expressing.dat",
    bedout="interactions_bin10kb_limb_expressing.bed",
    normout="normalized_bin10kb_expressing.dat";
  int bw=10000,
   Nbins;
  long int start=28000000,
    end=30000000;
 

  ifstream inf;
  ofstream ouf;
  string line;
  stringstream sline;

  double **M,
    **factor;
  int **counter;
  double mean_counter;

  string chrom1, chrom2;
  long int s1,s2, e1,e2, mid1,mid2;
  double v;
  int ii,jj;

  // set up region
  Nbins=ceil( double(end-start)/double(bw) );
  if (start+Nbins*bw != end) {
    cout<<"To give whole number of bins, end has been adjusted."<<endl;
    cout<<"   old value = "<<end<<"     new value = "<<start+Nbins*bw<<endl;
    end=start+Nbins*bw;
  }

  // set up arrays
  M = new double*[Nbins];
  factor = new double*[Nbins];
  counter = new int*[Nbins];
  for (int i=0;i<Nbins;i++) {
    M[i] = new double[Nbins];
    factor[i] = new double[Nbins];
    counter[i] = new int[Nbins];
    for (int j=0;j<Nbins;j++) {
      M[i][j] = 0.0;
      factor[i][j] = 0.0;
      counter[i][j] = 0;
    }
  }

  // open file and load lines
  inf.open( infile.c_str() );
  cout<<"Reading file "<<infile<<" ..."<<flush;
  getline(inf,line);
  do {

    // read line
    sline.clear();
    sline.str( line );
    sline>>chrom1>>s1>>e1>>chrom2>>s2>>e2>>v;

    // check size of fragment 1
    if ( e1-s1 > 2*bw && e2-s2 > 2*bw ) {

      int N1=floor( double(e1-s1)/double(bw) ),
	N2=floor( double(e2-s2)/double(bw) ),
	w1=floor(double(e1-s1)/double(N1)),
	w2=floor(double(e2-s2)/double(N2));

      vector<int> vmid1,vmid2;

      for (int i=s1+floor(0.5*w1);i<e1;i+=w1) {
	vmid1.push_back(i);
      }
      for (int i=s2+floor(0.5*w2);i<e2;i+=w2) {
	vmid2.push_back(i);
      }

      for (int i=0;i<vmid1.size();i++) {
	for (int j=0;j<vmid2.size();j++) {
	  ii=bp2bin(vmid1[i],start,bw);
	  jj=bp2bin(vmid2[j],start,bw);
	  if ( ii>=0&&ii<Nbins && jj>=0&&jj<Nbins ) {
	    M[ii][jj]+=v;
	    counter[ii][jj]++;
	  }
	}
      }

    } else if ( e1-s1 > 2*bw ) {

      int N1=floor( double(e1-s1)/double(bw) ),
	w1=floor(double(e1-s1)/double(N1));

      vector<int> vmid1;

      for (int i=s1+floor(0.5*w1);i<e1;i+=w1) {
	vmid1.push_back(i);
      }

      mid2 = floor( 0.5*double(s2+e2) );

      jj=bp2bin(mid2,start,bw);
      for (int i=0;i<vmid1.size();i++) {
	ii=bp2bin(vmid1[i],start,bw);
	if ( ii>=0&&ii<Nbins && jj>=0&&jj<Nbins ) {
	  M[ii][jj]+=v;
	  counter[ii][jj]++;
	}
      }

    } else if ( e2-s2 > 2*bw ) {

      int N2=floor( double(e2-s2)/double(bw) ),
	w2=floor(double(e2-s2)/double(N2));

      vector<int> vmid2;

      for (int i=s2+floor(0.5*w2);i<e2;i+=w2) {
	vmid2.push_back(i);
      }

      mid1 = floor( 0.5*double(s1+e1) );

      ii=bp2bin(mid1,start,bw);
      for (int j=0;j<vmid2.size();j++) {
	jj=bp2bin(vmid2[j],start,bw);
	if ( ii>=0&&ii<Nbins && jj>=0&&jj<Nbins ) {
	  M[ii][jj]+=v;
	  counter[ii][jj]++;
	}
      }
      

    } else {
      mid1 = floor( 0.5*double(s1+e1) );
      mid2 = floor( 0.5*double(s2+e2) );
      
      ii=bp2bin(mid1,start,bw);
      jj=bp2bin(mid2,start,bw);
      if ( ii>=0&&ii<Nbins && jj>=0&&jj<Nbins ) {
	M[ii][jj]+=v;
	counter[ii][jj]++;
      }

    }

  } while ( getline(inf,line) );
  inf.close();
  cout<<" Done."<<endl;

  // now output
  ouf.open( oufile.c_str() );
  cout<<"Writting file "<<oufile<<" ..."<<flush;
  //  ouf<<"# mid of bin1, mid of bin2, interactions"<<endl;
  for (int i=0;i<Nbins;i++) {
    for (int j=0;j<Nbins;j++) {
      ouf<<start+0.5*bw+i*bw<<" "<<start+0.5*bw+j*bw<<" "<<M[i][j]<<endl;
    }
    ouf<<endl;
  }
  ouf.close();
  cout<<" Done."<<endl;

  ouf.open( bedout.c_str() );
  cout<<"Writting file "<<bedout<<" ..."<<flush;
  for (int i=0;i<Nbins;i++) {
    for (int j=0;j<Nbins;j++) {
      ouf<<chrom1<<"\t"<<start+i*bw<<"\t"<<start+bw+i*bw<<"\t"
	 <<chrom2<<"\t"<<start+j*bw<<"\t"<<start+bw+j*bw<<"\t"<<M[i][j]<<endl;
    }
  }
  ouf.close();
  cout<<" Done."<<endl;

  mean_counter=0;
  {
    int c=0;
    for (int i=0;i<Nbins;i++) {
      for (int j=i;j<Nbins;j++) {
	if (counter[i][j]>0) {
	  mean_counter+=counter[i][j];
	  c++;
	}
      }
    }
    mean_counter/=double(c);
  }
  for (int i=0;i<Nbins;i++) {
    for (int j=0;j<Nbins;j++) {
      if (counter[i][j]>0) {
	factor[i][j] = mean_counter/double(counter[i][j]);
      } else {
	factor[i][j] = 1.0;
      }
    }
  }
  

  ouf.open( normout.c_str() );
  cout<<"Writting file "<<normout<<" ..."<<flush;
  //  ouf<<"# mid of bin1, mid of bin2, interactions"<<endl;
  for (int i=0;i<Nbins;i++) {
    for (int j=0;j<Nbins;j++) {
      ouf<<start+0.5*bw+i*bw<<" "<<start+0.5*bw+j*bw<<" "<<M[i][j]*factor[i][j]<<" "<<factor[i][j]<<endl;
    }
    ouf<<endl;
  }
  ouf.close();
  cout<<" Done."<<endl;

  ouf.open( ("normalized_"+bedout).c_str() );
  cout<<"Writting file "<<"normalized_"+bedout<<" ..."<<flush;
  for (int i=0;i<Nbins;i++) {
    for (int j=0;j<Nbins;j++) {
      ouf<<chrom1<<"\t"<<start+i*bw<<"\t"<<start+bw+i*bw<<"\t"
	 <<chrom2<<"\t"<<start+j*bw<<"\t"<<start+bw+j*bw<<"\t"<<M[i][j]*factor[i][j]<<endl;
    }
  }
  ouf.close();
  cout<<" Done."<<endl;
}




int bp2bin(const long int &bp,const long int &start,const int &bw) {
  // convert bp into bin number (counting from 0)
  return floor( double(bp-start)/double(bw) );
}
