
#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;


int main (int argc, char *argv[]) {

  double sigma=1.0,
    mu=0.0,
    z,
    ncdf,
    p;

  // get options from command line
  if (argc!=2) {
    cout<<"Error. Usage :"<<endl;
    cout<<"              ./z_to_p.out z"<<endl;
    cout<<"where z is a z-score."<<endl;
    exit(0);
  }

  // get abs(z)
  z=abs( atof(argv[1]) );

  // get normcdf
  ncdf = 0.5 * erfc( -(z-mu)/( sigma*sqrt(2.0) ) );

  if ( ncdf>1 ) {
    ncdf=1;
  }

  p = 2.0*( 1.0-ncdf );

  cout<<p<<endl;

}
